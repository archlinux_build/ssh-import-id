# ssh-import-id

Import SSH public keys from Launchpad or other URLs.

## Download

Download **ssh-import-id** archlinux package from here:

* [ssh-import-id-x86_64.pkg.tar.zst](https://gitlab.com/archlinux_build/ssh-import-id/-/jobs/artifacts/main/browse?job=run-build)
